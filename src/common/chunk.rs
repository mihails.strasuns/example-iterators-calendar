/// Represents iterator over a fixed length chunk subset of original iterator
#[derive(Clone)]
pub struct Chunk<T>
where
    T: Iterator,
{
    /// Clone of original iterator
    pub source: T,
    /// Amount of items to take from `self.source` for this chunk
    pub count: usize,
}

impl<T> Iterator for Chunk<T>
where
    T: Iterator,
{
    type Item = T::Item;

    fn next(&mut self) -> Option<Self::Item> {
        if self.count == 0 {
            None
        } else {
            self.count -= 1;
            self.source.next()
        }
    }
}

#[cfg(test)]
use itertools::assert_equal;

#[test]
fn test_chunk() {
    let data = vec![1, 2, 3, 4];
    let x = Chunk {
        source: data.iter(),
        count: 2,
    };
    assert_equal(x, [1, 2].iter());
}
