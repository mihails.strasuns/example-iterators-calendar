extern crate chrono;
extern crate itertools;

mod common;

mod dates_iterator;
mod split_adaptor;
mod chunk_of_adaptor;

fn main() {
    use dates_iterator::year_dates;
    use split_adaptor::SplitAdaptor;
    use chrono::Weekday;
    use chrono::offset::Utc;
    use chrono::{Date, Datelike};

    let months = year_dates(2017)
        .split(|a, b| a.month() != b.month())
        .map(|month| {
            month.split(
                |_, b| b.weekday() != Weekday::Mon,
            )
        });

    use chunk_of_adaptor::ChunksOfAdaptor;

    // Error:
    // the method `chunks_of` exists but the following trait bounds
    // were not satisfied:
    // `std::iter::Map<split_adaptor::Split<dates_iterator::YearDates,
    // [closure@src/main.rs:18:16: 18:45]>,
    // [closure@src/main.rs:19:14: 19:69]> : std::clone::Clone`

    // for row in months.chunks_of(3) {
    // }
}
