use common::chunk::Chunk;

/// Represents iterator adaptor that splits source iterator into fixed length
/// chunks
#[derive(Clone)]
pub struct Chunks<T>
where
    T: Clone + Iterator,
{
    input: T,
    count: usize,
}

impl<T> Iterator for Chunks<T>
where
    T: Clone + Iterator,
{
    type Item = Chunk<T>;

    fn next(&mut self) -> Option<Self::Item> {
        let ret = Chunk {
            source: self.input.clone(),
            count: self.count,
        };

        let mut empty = true;
        for _ in 0..self.count {
            if let Some(_) = self.input.next() {
                empty = false;
            }
        }

        if empty { None } else { Some(ret) }
    }
}

/// Defines `chunks_of` method for all cloneable iterators
pub trait ChunksOfAdaptor: Iterator + Clone {
    /// Returns iterator adaptor that interprets original iterator as sequence
    /// of fixed length chunks
    ///
    /// # Arguments
    /// * `count` - length of each chunk. Last chunk may be shorter if there is
    ///   not enough elements left in the underlying iterator
    fn chunks_of(&self, count: usize) -> Chunks<Self> {
        Chunks {
            input: self.clone(),
            count: count,
        }
    }
}

impl<T> ChunksOfAdaptor for T
where
    T: Clone + Iterator,
{
}


#[cfg(test)]
use itertools::assert_equal;

#[test]
fn test_chunks() {
    let v = vec![1, 2, 3, 4];
    let mut x = v.iter().chunks_of(2);

    let x1 = x.next().unwrap();
    assert_equal(x1, vec![1, 2].iter());

    let x2 = x.next().unwrap();
    assert_equal(x2, vec![3, 4].iter());

    assert!(x.next().is_none());
}
