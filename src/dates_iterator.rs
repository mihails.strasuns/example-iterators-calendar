use chrono::offset::Utc;
use chrono::Date;

/// Represents a range of dates for a single year such that one iteration
/// step matches one day
#[derive(Clone, Debug)]
pub struct YearDates {
    date: Option<Date<Utc>>,
}

impl Iterator for YearDates {
    type Item = Date<Utc>;

    fn next(&mut self) -> Option<Self::Item> {
        use chrono::Datelike;

        if self.date.is_none() {
            return None;
        }

        let curr = self.date.unwrap();
        let succ = curr.succ();
        if succ.year() != curr.year() {
            // collapse `self.date` into None after the last day of the year:
            self.date = None;
        } else {
            self.date = Some(succ);
        }

        Some(curr)
    }
}

/// Returns a date iterator for a given year
///
/// # Arguments
/// * `year` - year number to generate dates for
pub fn year_dates(year: i32) -> YearDates {
    use chrono::TimeZone;
    YearDates { date: Some(Utc.ymd(year, 1, 1)) }
}

#[test]
fn test_year_dates() {
    use chrono::TimeZone;

    assert!(year_dates(2000).next().unwrap() == Utc.ymd(2000, 1, 1));
    assert!(
        year_dates(2000)
            .skip(366)
            .next()
            .is_none()
    );
}
